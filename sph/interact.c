#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <omp.h>

#include "globalconfig.h"

#include "vec3.h"
#include "params.h"
#include "state.h"
#include "interact.h"

#include "zmorton.h"
#include "binhash.h"

/*@T
 * \subsection{Density computations}
 *
 * The formula for density is
 * \[
 *   \rho_i = \sum_j m_j W_{p6}(r_i-r_j,h)
 *          = \frac{315 m}{64 \pi h^9} \sum_{j \in N_i} (h^2 - r^2)^3.
 * \]
 * We search for neighbors of node $i$ by checking every particle,
 * which is not very efficient.  We do at least take advange of
 * the symmetry of the update ($i$ contributes to $j$ in the same
 * way that $j$ contributes to $i$).
 *@c*/

inline
void update_density(particle_t* pi, particle_t* pj, float h2, float C)
{
    float r2 = vec3_dist2(pi->x, pj->x);
    float z  = h2 - r2;
    if (z > 0) {
        float rho_ij = C*z*z*z;
        pi->rho += rho_ij;
        pj->rho += rho_ij;
    }
}

inline
void update_i_density(particle_t* pi, particle_t* pj, float h2, float C)
{
    float r2 = vec3_dist2(pi->x, pj->x);
    float z  = h2 - r2;
    if (z > 0) {
        float rho_ij = C*z*z*z;
        pi->rho += rho_ij;
    }
}

inline
void update_density_local(particle_t* pi, particle_t* pj, float h2, float C, particle_t* particles, float* local_storage)
{
    float r2 = vec3_dist2(pi->x, pj->x);
    float z  = h2 - r2;
    float* ri = local_storage + (pi - particles);
    float* rj = local_storage + (pj - particles);
    if (z > 0) {
        float rho_ij = C*z*z*z;
        *ri += rho_ij;
        *rj += rho_ij;
    }
}

inline
void get_bin_splits(sim_state_t* s, unsigned* bin_split){
    const unsigned num_threads = omp_get_num_threads();
    if (s->have_bin_inter==0){
        // we have no info on how many interactions are in each bin
        // so, just divide the bins up evenly
        for (unsigned p=0; p<=num_threads; p++){
            bin_split[p] = (p*HASH_SIZE) / num_threads;
        }
    }
    else {

        // we know how many interactions per bin there were on the previous step
        // so, divide the bins up according to interactions
        int total_inter=0;
        // sum up all the interactions
        for (int b_i=0; b_i<HASH_SIZE; b_i++) {
            total_inter += s->bin_inter[b_i];
        }
        // divide up the bins
        unsigned inter_count=0;
        unsigned bin_num = 0;
        for (int p=0; p<=num_threads; p++){
            while (inter_count <= (p*total_inter)/num_threads && bin_num<HASH_SIZE) {
                inter_count += s->bin_inter[bin_num];
                bin_num++;
            }
            bin_split[p] = bin_num;
        }
        bin_split[0]--;
    }
}


void compute_density(sim_state_t* s, sim_param_t* params)
{

    int          n    = s -> n;
    particle_t*  p    = s -> part;
    float        mass = s -> mass;
#ifdef USE_BUCKETING
    particle_t** hash = s -> hash;
#endif // USE_BUCKETING

    const float h  = params -> h;
    const float h2 = params -> h2;
    const float h3 = params -> h3;
    const float C  = ( 315.0/64.0/M_PI ) * mass / params -> h9;

    // store local density accumulation here
    // make it a little too big to avoid false sharing
    float *local_storage_r = (float*) calloc(n, sizeof(float));

    // Clear densities
    #pragma omp for nowait
    for (int i = 0; i < n; ++i)
        p[i].rho = 0;

    // Accumulate density info
#ifdef USE_BUCKETING
    /* BEGIN TASK */
#ifdef LOOP_BY_BUCKETS
    // Loop over all bins (Kyle)
    unsigned buckets[MAX_NBR_BINS]; // (27 or 8; dep on h or 2h bin size)
    unsigned num_buckets;

#ifdef USE_OMP
    // Cardinal Rule: only loop i alters bin i's data
    const unsigned my_i = omp_get_thread_num();
    unsigned bin_split[10]; // 10>omp_get_num_threads()+1
    get_bin_splits(s, bin_split);
    const unsigned my_min_bin = bin_split[my_i];
    const unsigned my_max_bin = bin_split[my_i+1];
    // we need this barrier -
    // if one processor finishes computing bin_split slightly earlier, then it
    // will move on.  When it gets to the following bin loop it will start
    // changing the interaction counts per bin, which will mess up the
    // processors that are still computing bin_split.
    #pragma omp barrier


    // loop over each bin
    for (int b_i=my_min_bin; b_i < my_max_bin; ++b_i) {
        // loop over each particle in our bin
        particle_t* p_i = hash[b_i];
        unsigned bin_interactions = 0u;
        // if our bin isn't empty
        if (p_i != NULL) {
            num_buckets = particle_neighborhood(buckets, p_i, h);
            particle_t* prev = p_i;
            while (p_i != NULL) {
                if (same_bucket(p_i,prev,h)==0)
                    num_buckets = particle_neighborhood(buckets, p_i, h);
                prev = p_i;

                p_i->rho += ( 315.0/64.0/M_PI ) * mass / h3;
                // First, check interaction with particles that ARE in this bin
                // Only look forwards to avoid duplicate symmetric work
                particle_t* p_j = p_i -> next;
                while (p_j != NULL) {
                    bin_interactions ++;
                    update_density_local(p_i, p_j, h2, C, p, local_storage_r);
                    p_j = p_j -> next;
                }

                // loop through all neighbor bins to b_i
                for (int cb_j = 0; cb_j < num_buckets; cb_j++) {
                    int b_j = buckets[cb_j];

                    // if b_j is owned by a different processor
                    //if (b_j < my_min_bin || b_j >= my_max_bin) {
                    if (0) {
                        p_j = hash[b_j];
                        while (p_j != NULL) {
                            bin_interactions++;
                            update_density_local(p_i, p_j, h2, C, p, local_storage_r);
                            p_j = p_j -> next;
                        }
                    }
                    // if b_j is owned by the same processor
                    //else if (b_j > b_i) {
                    if (b_j > b_i) {
                        p_j = hash[b_j];
                        while (p_j != NULL) {
                            bin_interactions++;
                            update_density_local(p_i, p_j, h2, C, p, local_storage_r);
                            p_j = p_j -> next;
                        }
                    }
                }
                #pragma omp master
                    s->have_bin_inter = 1;
                p_i = p_i -> next;
            }
        }
        s->bin_inter[b_i] = bin_interactions;
    }
    //#pragma omp barrier

    //#pragma omp critical
    //for (int i=0; i<n; ++i) {
    //    (p+i)->rho += local_storage_r[i];
    //}

    // a better version would split the sum into chunks
    unsigned nthreads = omp_get_num_threads();
    for (int i=0; i<nthreads; ++i) {
        #pragma omp barrier
        unsigned chunk = (i + omp_get_thread_num()) % nthreads;
        unsigned start_idx = (chunk * n) / nthreads;
        unsigned stop_idx = ((chunk+1) * n) / nthreads;
        for (int j = start_idx; j<stop_idx; ++j) {
            (p+j)->rho += local_storage_r[j];
        }
    }
    #pragma omp barrier



#else // if not USE_OMP

    // loop over each bin
    #pragma omp single
    for (int b_i = 0; b_i < HASH_SIZE; ++b_i){
        // loop over each particle in bin b_i
        particle_t* p_i = hash[b_i];
        if (p_i != NULL) {
            num_buckets = particle_neighborhood(buckets, p_i, h);
            particle_t* prev = p_i;
            while (p_i != NULL) {
                if (same_bucket(p_i,prev,h)==0)
                    num_buckets = particle_neighborhood(buckets, p_i, h);
                prev = p_i;

                p_i->rho += ( 315.0/64.0/M_PI ) * mass / h3;
                // First, check interaction with particles that ARE in this bin
                particle_t* p_j = p_i -> next;
                while (p_j != NULL) {
                    update_density(p_i, p_j, h2, C);
                    p_j = p_j -> next;
                }
                // Second, check interaction with particles NOT in this bin
                // loop through each neighbor bin
                for (int cb_j = 0; cb_j < num_buckets; cb_j++) {
                    int b_j = buckets[cb_j];
                    if (b_j > b_i) { // this way we don't double count
                        // loop over each particle
                        p_j = hash[b_j];
                        while (p_j != NULL) {
                            update_density(p_i, p_j, h2, C);
                            p_j = p_j -> next;
                        }
                    }
                }
                p_i = p_i -> next;
            }
        }
    }
#endif // if not USE_OMP
#else // LOOP OVER PARTICLES (!defined LOOP_BY_BUCKETS)
    // Loop over all particles
    #pragma omp for
    for (int i = 0; i < n; i ++) {
        particle_t* pi = p + i;
        pi->rho += ( 315.0/64.0/M_PI ) * s->mass / h3;
        // Get neighboring bin info:
        unsigned buckets[MAX_NBR_BINS]; // 27 or 7 (h or 2h bins)
        unsigned num_buckets = particle_neighborhood(buckets, pi, h);
        // Loop over individual buckets:
        for (unsigned j = 0; j < num_buckets; j ++) {
            particle_t* pj = hash[buckets[j]];
            while (pj != NULL) {
                if (pj != pi) {
                    update_i_density(pi, pj, h2, C);
                }
                pj = pj -> next;
            }
        }
    }
#endif // LOOP_BY_BUCKETS
    /* END TASK */
#else // !defined USE_BUCKETING

    for (int i = 0; i < n; ++i) {
        particle_t* pi = p + i;
        pi->rho += ( 315.0/64.0/M_PI ) * s->mass / h3;
        for (int j = i+1; j < n; ++j) {
            particle_t* pj = p + j;
            update_density(pi, pj, h2, C);
        }
    }
#endif // USE_BUCKETING
    free(local_storage_r);
}


/*@T
 * \subsection{Computing forces}
 *
 * The acceleration is computed by the rule
 * \[
 *   \bfa_i = \frac{1}{\rho_i} \sum_{j \in N_i}
 *     \bff_{ij}^{\mathrm{interact}} + \bfg,
 * \]
 * where the pair interaction formula is as previously described.
 * Like [[compute_density]], the [[compute_accel]] routine takes
 * advantage of the symmetry of the interaction forces
 * ($\bff_{ij}^{\mathrm{interact}} = -\bff_{ji}^{\mathrm{interact}}$)
 * but it does a very expensive brute force search for neighbors.
 *@c*/

inline
void update_forces(particle_t* pi, particle_t* pj, float h2,
                   float rho0, float C0, float Cp, float Cv)
{
    float dx[3];
    vec3_diff(dx, pi->x, pj->x);
    float r2 = vec3_len2(dx);
    if (r2 < h2) {
        const float rhoi = pi->rho;
        const float rhoj = pj->rho;
        const float q  = sqrt(r2/h2);
        const float u  = 1-q;
        const float w0 = C0 * u/rhoi/rhoj;
        const float wp = w0 * Cp * (rhoi+rhoj-2*rho0) * u/q;
        const float wv = w0 * Cv;
        float dv[3];
        vec3_diff(dv, pi->v, pj->v);

        // Equal and opposite pressure forces
        vec3_saxpy(pi->a,  wp, dx);
        vec3_saxpy(pj->a, -wp, dx);

        // Equal and opposite viscosity forces
        vec3_saxpy(pi->a,  wv, dv);
        vec3_saxpy(pj->a, -wv, dv);
    }
}

inline
void update_i_forces(particle_t* pi, particle_t* pj, float h2,
                     float rho0, float C0, float Cp, float Cv)
{
    float dx[3];
    vec3_diff(dx, pi->x, pj->x);
    float r2 = vec3_len2(dx);
    if (r2 < h2) {
        const float rhoi = pi->rho;
        const float rhoj = pj->rho;
        const float q  = sqrt(r2/h2);
        const float u  = 1-q;
        const float w0 = C0 * u/rhoi/rhoj;
        const float wp = w0 * Cp * (rhoi+rhoj-2*rho0) * u/q;
        const float wv = w0 * Cv;
        float dv[3];
        vec3_diff(dv, pi->v, pj->v);

        // Equal and opposite pressure forces
        vec3_saxpy(pi->a,  wp, dx);
        //vec3_saxpy(pj->a, -wp, dx);

        // Equal and opposite viscosity forces
        vec3_saxpy(pi->a,  wv, dv);
        //vec3_saxpy(pj->a, -wv, dv);
    }
}

inline
void update_forces_local(particle_t* pi, particle_t* pj, float h2,
                   float rho0, float C0, float Cp, float Cv,
                   particle_t* particles, float* local_storage)
{
    float dx[3];
    vec3_diff(dx, pi->x, pj->x);
    float r2 = vec3_len2(dx);
    if (r2 < h2) {
        const float rhoi = pi->rho;
        const float rhoj = pj->rho;
        const float q  = sqrt(r2/h2);
        const float u  = 1-q;
        const float w0 = C0 * u/rhoi/rhoj;
        const float wp = w0 * Cp * (rhoi+rhoj-2*rho0) * u/q;
        const float wv = w0 * Cv;
        float dv[3];
        vec3_diff(dv, pi->v, pj->v);

        float* ai = local_storage + 3*(pi - particles);
        float* aj = local_storage + 3*(pj - particles);

        // Equal and opposite pressure forces
        vec3_saxpy(ai,  wp, dx);
        vec3_saxpy(aj, -wp, dx);

        // Equal and opposite viscosity forces
        vec3_saxpy(ai,  wv, dv);
        vec3_saxpy(aj, -wv, dv);
    }
}

void compute_accel(sim_state_t* state, sim_param_t* params)
{

    // Unpack basic parameters
    const float h    = params -> h;
    const float rho0 = params -> rho0;
    const float k    = params -> k;
    const float mu   = params -> mu;
    const float g    = params -> g;
    const float mass = state  -> mass;
    const float h2   = h * h;

    // Unpack system state
    particle_t*  p    = state -> part; // part is list of particles
#ifdef USE_BUCKETING
    particle_t** hash = state -> hash;
#endif // USE_BUCKETING
    const int n = state -> n;

    // store local acceleration accumulation here
    // make it a little too big to avoid false sharing
    float *local_storage_a = (float*) calloc(3*n, sizeof(float));

    // Rehash the particles
    hash_particles(state, h);

    // Compute density and color
    compute_density(state, params);

    // Start with gravity and surface forces
    #pragma omp for nowait
    for (int i = 0; i < n; ++i)
        vec3_set(p[i].a,  0, -g, 0);

    // Constants for interaction term
    const float C0 = 45 * mass / M_PI / ( (h2)*(h2)*h );
    const float Cp = k/2;
    const float Cv = -mu;

    // Accumulate forces
#ifdef USE_BUCKETING
    /* BEGIN TASK */
#ifdef LOOP_BY_BUCKETS
    // Loop over bins
    unsigned buckets[MAX_NBR_BINS]; // (27 or 7; dep on h or 2h bin size)
    unsigned num_buckets;

#ifdef USE_OMP
    const unsigned my_i = omp_get_thread_num();
    unsigned bin_split[10]; // 10>omp_get_num_threads()+1
    get_bin_splits(state, bin_split);
    const unsigned my_min_bin = bin_split[my_i];
    const unsigned my_max_bin = bin_split[my_i+1];

    #pragma omp barrier

    for (int b_i=my_min_bin; b_i < my_max_bin; ++b_i) {
        // loop over each particle in bin b_i
        particle_t* p_i = hash[b_i];
        if (p_i != NULL) {
            num_buckets = particle_neighborhood(buckets, p_i, h);
            particle_t* prev = p_i;
            while (p_i != NULL) {
                if (same_bucket(p_i,prev,h)==0)
                    num_buckets = particle_neighborhood(buckets, p_i, h);
                prev = p_i;

                // First, check interaction with particles that ARE in this bin
                particle_t* p_j = p_i->next;
                while (p_j != NULL) {
                    //update_forces(p_i, p_j, h2, rho0, C0, Cp, Cv);
                    update_forces_local(p_i, p_j, h2, rho0, C0, Cp, Cv, p, local_storage_a);
                    p_j = p_j -> next;
                }

                // Second, check interaction with particles that are NOT in this bin
                for (int cb_j = 0; cb_j < num_buckets; cb_j++) {
                    int b_j = buckets[cb_j];

                    // if nbr bin b_j is owned by a different processor:
                    //if (b_j < my_min_bin || b_j >= my_max_bin) {
                    //if (0) {
                    //    p_j = hash[b_j];
                    //    while (p_j != NULL) {
                    //        update_forces_local(p_i, p_j, h2, rho0, C0, Cp, Cv, p, local_storage_a);
                    //        p_j = p_j -> next;
                    //    }
                    //}
                    // if nbr bin b_j is owned by the same processor
                    //else if (b_j > b_i) {
                    if (b_j > b_i) {
                        p_j = hash[b_j];
                        while (p_j != NULL) {
                            //update_i_forces(p_i, p_j, h2, rho0, C0, Cp, Cv);
                            update_forces_local(p_i, p_j, h2, rho0, C0, Cp, Cv, p, local_storage_a);
                            p_j = p_j -> next;
                        }
                    }
                }
                p_i = p_i -> next;
            }
        }
    }

    // a better version would split the sum into chunks
    unsigned nthreads = omp_get_num_threads();
    for (int i=0; i<nthreads; ++i) {
        #pragma omp barrier
        unsigned chunk = (i + omp_get_thread_num()) % nthreads;
        unsigned start_idx = (chunk * n) / nthreads;
        unsigned stop_idx = ((chunk+1) * n) / nthreads;
        for (int j = start_idx; j<stop_idx; ++j) {
            vec3_saxpy(p[j].a, 1, local_storage_a + j*3);
        }
    }


    //#pragma omp critical
    //for (int i=0; i<n; ++i) {
    //    vec3_saxpy(p[i].a, 1, local_storage_a + i*3);
    //}
    #pragma omp barrier

#else // not def USE_OMP
    for (int b_i=0; b_i<HASH_SIZE; ++b_i){
        // loop over each particle in bin b_i
        particle_t* p_i = hash[b_i];
        if (p_i != NULL) {
            num_buckets = particle_neighborhood(buckets, p_i, h);
            particle_t* prev = p_i
            while (p_i != NULL) {
                if (same_bucket(p_i,prev,h)==0)
                    num_buckets = particle_neighborhood(buckets, p_i, h);
                prev = p_i;

                // First, check interaction with particles that ARE in this bin
                particle_t* p_j = p_i->next;
                while (p_j != NULL) {
                    update_forces(p_i, p_j, h2, rho0, C0, Cp, Cv);
                    p_j = p_j -> next;
                }
                // Second, check interaction with particles that are NOT in this bin
                // loop through each neighbor bin
                for (int cb_j = 0; cb_j < num_buckets; cb_j++) {
                    int b_j = buckets[cb_j];
                    if (b_j > b_i) { // this way we don't double count
                        // loop over each particle
                        p_j = hash[b_j];
                        while (p_j != NULL) {
                            update_forces(p_i, p_j, h2, rho0, C0, Cp, Cv);
                            p_j = p_j -> next;
                        }
                    }
                }
                p_i = p_i -> next;
            }
        }
    }
#endif // USE_OMP
#else // LOOP OVER PARTICLES (!defined LOOP_BY_BUCKETS)
    // Loop over particles
    #pragma omp for
    for (int i = 0; i < n; ++i) {
        particle_t* pi = p + i;
        // Get neighboring bin info:
        unsigned buckets[MAX_NBR_BINS]; // 27 or 7 (h or 2h bins)
        unsigned num_buckets = particle_neighborhood(buckets, pi, h);
        // Loop over individual buckets:
        for (unsigned j = 0; j < num_buckets; j ++) {
            // For bucket j go thru the linked list:
            particle_t* pj = hash[buckets[j]];
            while (pj != NULL) {
                if (pj != pi) {
                    update_i_forces(pi, pj, h2, rho0, C0, Cp, Cv);
                }
                pj = pj -> next;
            }
        }
    }
#endif // LOOP_BY_BUCKETS

    /* END TASK */
#else // !defined USE_BUCKETING

    for (int i = 0; i < n; ++i) {
        particle_t* pi = p + i;
        for (int j = i+1; j < n; ++j) {
            particle_t* pj = p + j;
            update_forces(pi, pj, h2, rho0, C0, Cp, Cv);
        }
    }
#endif // USE_BUCKETING
    free(local_storage_a);
}

