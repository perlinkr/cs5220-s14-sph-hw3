#ifndef _globalconfig_h
#define _globalconfig_h

/*
 * This file contains macros that can turn on/off any changes we make to the base code.
 * Be sure to include it at the top of any file you use any of these macros in.
 *
 * To make the code easier to read please comment #endif's (and #else's) with whatever the option is.
 * For example:
 *      #ifdef SWIRL
 *      __code_here___
 *      #endif // SWIRL
 */

typedef unsigned bool;
#define true  1
#define false 0

/** Define this to use OpenMP
 *
 * This modifies interact.c
 *
 */
#define USE_OMP

/** Define this to use the bucketing version of the code
 *
 * This modifies binhash.c and interact.c
 *
 * USE_BUCKETING:   turns on bucketing
 * LOOP_BY_BUCKETS: when updating, loops over buckets (if off, loops over particles)
 *                  experimentally it is faster to loop over buckets (in serial)
 */
#define USE_BUCKETING
#define LOOP_BY_BUCKETS

/** Define this to optimize various functions
 *
 * OPT_BIN_NBR: optimize the particle_neighbors() function (goes from 20% total time to ??%)
 *
 */
#define OPT_BIN_NBR

/** Define this to use a "swirling effect"
 *
 * This modifies leapfrog.c.
 */
// #define SWIRL

/** Define this to use a "spraying effect"
 *
 * This modifies leapfrog.c.
 */
// #define SPRAY

#endif // header endif
