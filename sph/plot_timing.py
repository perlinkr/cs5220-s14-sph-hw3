#!/usr/bin/env python

import matplotlib
matplotlib.use('Agg') # magic backend for plots without $DISPLAY
import matplotlib.pyplot as plt

import argparse
import numpy as np

def make_cli():
    parser = argparse.ArgumentParser(description="plot a timing file")
    parser.add_argument('timing_file', help='file with problem sizes and times')
    parser.add_argument('plot_file', help='where to write the output plot to')
    return parser.parse_args()

if __name__ == '__main__':
    args = make_cli()
    x,t = np.loadtxt(args.timing_file, skiprows=1, unpack=True)

    perm = np.argsort(x)
    x = x[perm]
    t = t[perm]

    fig = plt.figure(figsize=(4,4))
    ax = fig.add_subplot(111)
    ax.plot(x,t)
    ax.set_xlabel('Number of particles')
    ax.set_ylabel('Run time')
    fig.tight_layout()
    fig.savefig(args.plot_file)
