#!/usr/bin/env python

##############################################################
# scaling_tester.py
#
# This is a scripting wrapper to do multiple runs of sph.x
# with different parameter settings, and then summarize the 
# results. It seemed unreasonably complicated to try to write
# a command line interface. 
#
# Instead, the parts you might want to adjust to set up a run
# are in the super-clearly marked section below. Everyerything
# else is just scaffolding, and should work.
#
# -Kyle W
##############################################################

import subprocess
import argparse
import shutil
import glob
import time
import pwd
import os
import re

def make_cli():
    parser = argparse.ArgumentParser(description='script to run sph.x on a range of data and collect the results')
    parser.add_argument('timing_results_dir', help='where to put the timing files and results')
    return parser.parse_args()

def get_username():
    return pwd.getpwuid(os.getuid())[0]

def get_num_jobs_running():
    """Call "condor_q <username>" and parse the output to find out how many jobs this user is running"""
    data = subprocess.Popen(['condor_q', get_username()], stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT).communicate()[0]
    lastline = data.splitlines()[-1]
    num_jobs = int(lastline.split()[0])
    return num_jobs

if __name__ == '__main__':
    # get the command line args. Right now this is just a dir to write everything to
    args = make_cli()
    timing_output_dir = args.timing_results_dir

    # we're going to be making a lot of csub files. If there are existing csub files lying around that
    # would be really confusing. I don't like deleting files in scripts, so just warn the user and exit.
    # the same thing counts for run_*.out files.
    if len(glob.glob('./csub*')):
        print '[scaling_tester] There are existing csub files! Delete these, and then try running this again'
        exit(1)
    if len(glob.glob('./ompsub*')):
        print '[scaling_tester] There are existing ompsub files! Delete these, and then try running this again'
        exit(1)
    if len(glob.glob('./run_*.out')):
        print '[scaling_tester] There are existing run_*.out files! Delete these, and then try running this again'
        exit(1)

    # make sure the user doesn't already have jobs running, because that will confuse the code
    if get_num_jobs_running():
        print '[scaling_tester] it looks like you already have jobs running. Let them finish and try again later.'
        exit(1)

    # we'll be copying all the files to the timing_output_dir. If it already exists and isn't empty, then abort.
    if os.path.exists(timing_output_dir):
        if os.listdir(timing_output_dir):
            print '[scaling_tester] the specified timing outpur dir isnt empty. Please specify an empty location.'
            exit(1)
    else:
        os.makedirs(timing_output_dir)
    timing_out_file = os.path.join(timing_output_dir,'./timing.txt')

    ############################################
    ### Stuff you might want to adjust below ###
    ############################################

    # these are the default simulation parameters
    outfile = 'run.out'     # output file name
    nframes = 400           # number of frames in sim
    npframe = 100           # number of time steps per frame
    dt      = 1e-4          # time step
    s       = 5e-2          # particle size (determines number of particles)
    d       = 1000          # reference density
    k       = 1000          # bulk modulus
    v       = 0.1           # dynamic viscosity
    g       = 9.8           # gravitational strength 
    nthreads = 1

    # redefine some of these for a run.
    # for example, this uses all of the values above, except that it ignores the 's' above and instead does a
    # run for each number in a tuple of values:
    nthreads = 2
    nframes = 2
    npframe = 2
    for i,s in enumerate((0.2,)):
        outfile = 'run_{0:03d}.out'.format(i)

        ###########################################
        ### End: Stuff you might want to adjust ###
        ###########################################
        
        # run this simulation by calling csub
        #print '[scaling_tester] submitting job {}'.format(i)
        #subprocess.call(['csub', './sph.x', '-o', outfile, '-F', str(nframes), '-f', str(npframe), 
        #    '-t', str(dt), '-s', str(s), '-d', str(d), '-k', str(k), '-v', str(v), '-g', str(g)])
        
        # run this simulation by calling ompsub
        print '[scaling_tester] submitting job {}'.format(i)
        subprocess.call(['ompsub', '-n', str(nthreads), './sph.x', '-o', outfile, '-F', str(nframes), 
            '-f', str(npframe), '-t', str(dt), '-s', str(s), '-d', str(d), '-k', str(k), 
            '-v', str(v), '-g', str(g)])


    # wait for the job to finish
    print 'waiting for the jobs to finish'
    num_jobs = 100000 # really big number
    while num_jobs:
        time.sleep(5)
        num_jobs_new = get_num_jobs_running()
        if num_jobs_new < num_jobs:
            print '[scaling_tester] {} jobs still running'.format(num_jobs_new)
            num_jobs = num_jobs_new

    # parsing the output files: get the run time from the csub*-o.0 file and the number of points from the '.out'
    print '[scaling_tester] parsing the run result files'
    runtimes = []
    num_points = []
    for sub_info in glob.glob('./*sub-*.sub'):
        # figure out which output file goes with this run
        p = re.compile('run_\d\d\d.out')
        with open(sub_info) as f:
            outfile = p.findall(f.read())[0]
        #stdoutfile = sub_info.replace('.sub', '-o.0')
        stdoutfile = sub_info.replace('.sub', '.o')
        with open(outfile) as f:
            line = f.readline()
            num_points.append(int(line.split()[1]))
        with open(stdoutfile) as f:
            line = ''
            for l in f: # silly way of getting last line
                line = l
            runtimes.append(float(line.split()[2]))
    
    # write the problem sizes and runtimes to a file
    with open(timing_out_file, 'w') as f:
        f.write('npts t\n')
        for x, t in zip(num_points, runtimes):
            f.write('{} {}\n'.format(x,t))
            
    # clean up: copy all csub and run_*.out files to the timing output dir
    print '[scaling_tester] moving all the run files to the timing folder'
    for f in glob.glob('./csub*'):
        shutil.move(f, timing_output_dir)
    for f in glob.glob('./ompsub*'):
        shutil.move(f, timing_output_dir)
    for f in glob.glob('./run_*.out'):
        shutil.move(f, timing_output_dir)
    
    # make a plot of size vs time
    print '[scaling_tester] drawing a plot of the results'
    plot_file = os.path.join(timing_output_dir, 'timing.eps')
    subprocess.call(['./plot_timing.py', timing_out_file, plot_file])
        
    print '[scaling_tester] done! all the results should be at:', timing_output_dir
