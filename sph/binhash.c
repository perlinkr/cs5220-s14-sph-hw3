#include <string.h>
#include <assert.h>

#include "globalconfig.h"

#include "zmorton.h"
#include "binhash.h"

/*@q
 * ====================================================================
 */

/*@T
 * \subsection{Spatial hashing implementation}
 *
 * In the current implementation, we assume [[HASH_DIM]] is $2^b$,
 * so that computing a bitwise of an integer with [[HASH_DIM]] extracts
 * the $b$ lowest-order bits.  We could make [[HASH_DIM]] be something
 * other than a power of two, but we would then need to compute an integer
 * modulus or something of that sort.
 *
 *@c*/

#define HASH_MASK (HASH_DIM-1)

unsigned particle_bucket_location(unsigned ix, unsigned iy, unsigned iz)
{
    return zm_encode(ix & HASH_MASK, iy & HASH_MASK, iz & HASH_MASK);
}

unsigned particle_bucket(particle_t* p, float h)
{
    unsigned ix = (p -> x[0]) / h;
    unsigned iy = (p -> x[1]) / h;
    unsigned iz = (p -> x[2]) / h;
    return particle_bucket_location(ix, iy, iz);
//    return zm_encode(ix & HASH_MASK, iy & HASH_MASK, iz & HASH_MASK);
}

// check if two particles are actually in the same bucket or not
unsigned same_bucket(particle_t* p1, particle_t* p2, float h) {
    unsigned ix1 = (p1 -> x[0]) / h;
    unsigned iy1 = (p1 -> x[1]) / h;
    unsigned iz1 = (p1 -> x[2]) / h;
    unsigned ix2 = (p2 -> x[0]) / h;
    unsigned iy2 = (p2 -> x[1]) / h;
    unsigned iz2 = (p2 -> x[2]) / h;
    return ( (ix1 == ix2) && (iy1 == iy2) && (iz1 == iz2) );
}

/**
 * particle_neighborhood(unsigned* buckets, particle_t* p, float h)
 *
 * Returns:    num of bins needed to updated particle p (sitting in a some bin)
 * Output arg: buckets returns which bins p needs to use
 * Input args: pointer to particle, radius of particles
 */

#ifdef OPT_BIN_NBR
bool interior_bucket(unsigned ix, unsigned iy, unsigned iz, float h) {
    unsigned M = 1 + 1.0 / h;
    return (ix > 0 && iy > 0 && iz > 0 &&
            ix < M && iy < M && iz < M);
}

unsigned particle_neighborhood_sides(unsigned* buckets, int ix, int iy, int iz,
        int dxmin, int dxmax, int dymin, int dymax, int dzmin, int dzmax) {
    unsigned num_bins = 0;
    for         (int dx = dxmin; dx <= dxmax; dx ++) {
        for     (int dy = dymin; dy <= dymax; dy ++) {
            for (int dz = dzmin; dz <= dzmax; dz ++) {
                unsigned nx = ix + dx;
                unsigned ny = iy + dy;
                unsigned nz = iz + dz;
                buckets[num_bins] = particle_bucket_location(nx, ny, nz);
                num_bins ++;
            }
        }
    }
    return num_bins;
}

unsigned particle_neighborhood(unsigned* buckets, particle_t* p, float h)
{
    unsigned num_bins = 0;
    unsigned ix = (p -> x[0]) / h;
    unsigned iy = (p -> x[1]) / h;
    unsigned iz = (p -> x[2]) / h;
    if (interior_bucket(ix, iy, iz, h)) {
        for         (int dx = -1; dx <= 1; dx ++) {
            for     (int dy = -1; dy <= 1; dy ++) {
                for (int dz = -1; dz <= 1; dz ++) {
                    unsigned nx = ix + dx;
                    unsigned ny = iy + dy;
                    unsigned nz = iz + dz;
                    buckets[num_bins] = particle_bucket_location(nx, ny, nz);
                    num_bins ++;
                }
            }
        }
        return num_bins;
    }
    else {
        // this particle is on a boundary
        // we need to figure out the 1 to 3 sides it's touching
        unsigned maxB1 = 1 + 1.0 / h;
        int dxmin=-1, dxmax=1, dymin=-1, dymax=1, dzmin=-1, dzmax=1;

        if (ix == 0)
            dxmin = 0;
        else if (ix == maxB1)
            dxmax = 0;
        if (iy == 0)
            dymin = 0;
        else if (iy == maxB1)
            dymax = 0;
        if (iz == 0)
            dzmin = 0;
        else if (iz == maxB1)
            dzmax = 0;

        return particle_neighborhood_sides(buckets, ix, iy, iz, dxmin, dxmax, dymin, dymax, dzmin, dzmax);
    }
}
#else // OPT_BIN_NBR (!defined OPT_BIN_NBR)
unsigned particle_neighborhood(unsigned* buckets, particle_t* p, float h)
{
    /* BEGIN TASK */
    // For now our naive implementation is to assume there are 27 neighbors
    unsigned num_bins = 0;
    int ix = (p -> x[0]) / h;
    int iy = (p -> x[1]) / h;
    int iz = (p -> x[2]) / h;
    unsigned maxB1 = 1 + 1.0 / h;
    // ^^^^ max (nx, ny, nz) can each be -- not sure why +1 needed but w/e it works LOL
    for         (int dx = -1; dx <= 1; dx ++) {
        for     (int dy = -1; dy <= 1; dy ++) {
            for (int dz = -1; dz <= 1; dz ++) {
                // now get the local bucket index
                // [(0,0,0)=lowerleft, (m,m,m)=topright for some m]
                int nx = ix + dx;
                int ny = iy + dy;
                int nz = iz + dz;
                if (nx >= 0     && ny >= 0      && nz >= 0 &&
                    nx <= maxB1 && ny <= maxB1  && nz <= maxB1) {
                    int temp_num = particle_bucket_location(nx, ny, nz);
                    if (temp_num > HASH_SIZE)
                        assert(0 == 1);
                    buckets[num_bins] = temp_num;
                    num_bins ++;
                }
            }
        }
    }
    return num_bins;
    /* END TASK */
}
#endif // OPT_BIN_NBR

/**
 * hash_particles(sim_state_t* s, float h)
 *
 * The state s contains the information about the problem.
 * - hash is an attribute of s that is a "list of pointers"
 *   each particle struct has the "next" attribute, which creates a linked list
 *
 * The purpose of this function is to set hash[i] for all i=0,1,...,HASH_SIZE-1
 * to some pointer located in each bin (physically defined). Each pointer then
 * creates a linked list using it's "next"
 *
 * We must first set all "next" for all particles to be NULL, and then make sure
 * that hash[i]=NULL for all i.
 */
void hash_particles(sim_state_t* s, float h)
{
    /* BEGIN TASK */
    // Localize variables:
    int n = s -> n;
    particle_t*  p    = s -> part;
    particle_t** hash = s -> hash;

    // Reset "next" for all particles:
    #pragma omp for nowait
    for (int i = 0; i < n; i ++) {
        particle_t* pi = p + i;
        pi -> next = NULL;
    }

    // Reset hash[i] for all i
    #pragma omp for
    for (int i = 0; i < HASH_SIZE; i ++) {
        hash[i] = NULL;
    }

    // Build the bins:
    #pragma omp single
    for (int i = 0; i < n; i ++) {
        particle_t* pi = p + i;
        unsigned bin_i = particle_bucket(pi, h);
        pi -> next     = hash[bin_i];
        hash[bin_i]    = pi;

    }
    /* END TASK */
}
